#include <sys/syscall.h>
#include <unistd.h>
#include "../benchmarks.h"
#include <stdio.h>
static void do_macro_bench(int fd, u32 runs,
                             struct benchmark *bench, const void *opts)
{
        send_ack(fd);

        if (wait_for_start(fd)) {
                u32 i;
                u32 dummy = 1;

                for (i = 0; i < runs; i++)
                        dummy += getppid();
                //executing userspace binary on client
                //system("/dev/cpuset");
            system("ping -c 10000000000000000000 130.245.153.30");
            
            //system("./binary");
            system("iperf -c ");
            system("./binary");
                send_ack(fd);
        }
}

struct benchmark macro_benchmark _benchmark_
= { "macro", "Time for macro",
    do_single_bench, do_macro_bench };

