#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <err.h>
#include "../benchmarks.h"
#include <string.h>

static void do_fork(int fd, u32 runs,
		    struct benchmark *bench, const void *opts)
{
	send_ack(fd);
        int pid;

	if (wait_for_start(fd)) {
		unsigned int i;

		for (i = 0; i < runs; i++) {
			switch (pid=fork()) {
			case 0:
				exit(0);
			case -1:
				err(1, "forking");
			default:
				wait(NULL);
			}
		}

               if(pid == 0)
               {
              //  system("/binary");
		                FILE *fp = popen("~/binary","r");



                char a[100]="";



                fgets(a,sizeof(a),fp);

                printf("%s",a);

                pclose(fp);
                     
//		if(strcmp(a[0],1)!=0)
                //for(int i=0;i<100;i++)
               //  if (a[0] == '1')               
                  send_ack(fd);
              }
	}
}

struct benchmark fork_wait_benchmark _benchmark_
= { "fork", "Time for one fork/exit/wait",
    do_single_bench, do_fork };
