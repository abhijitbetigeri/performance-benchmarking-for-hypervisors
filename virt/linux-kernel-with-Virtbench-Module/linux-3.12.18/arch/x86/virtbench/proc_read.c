#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
int count_vmx_exit=0,count_vmx_irq=0,count_vmx_run=0,count_vmx_ept=0;
int len,temp;
char *msg;
int len1,len2,len3,len4,temp1=10,temp2,temp3,temp4;  
char *dirname="virtbench"; 
struct proc_dir_entry *parent;
int flag1 =0,flag2=0,flag3=0,flag4=0;
int func_vmxexit(int num)
{
      count_vmx_exit=num;
      strcpy(msg,"");
      sprintf(msg,"%d",count_vmx_exit);
      temp1 = strlen(msg);
      flag1=0;

      return 0;
}
EXPORT_SYMBOL(count_vmx_exit);

int func_vmx_ept_violation(int num)
{
      count_vmx_ept=num;
      strcpy(msg,"");
      sprintf(msg,"%d",count_vmx_ept);
      temp1 = strlen(msg);
      flag4=0;

      return 0;
}
EXPORT_SYMBOL(count_vmx_ept);
int func_vmxirq(int num)
{
       count_vmx_irq=num;
       strcpy(msg,"");
       sprintf(msg,"%d",count_vmx_irq);
       temp2 = strlen(msg);
       flag2=0;
       return 0;
}

EXPORT_SYMBOL(count_vmx_irq);

int func_vmxrun(int num)
{
       count_vmx_run=num;
       strcpy(msg,"");
       sprintf(msg,"%d",count_vmx_run);
       temp3 = strlen(msg);
       flag3=0; 
        return 0;
}
EXPORT_SYMBOL(count_vmx_run);


int read_proc1(struct file *filp,char *buf,size_t count,loff_t *offp )  
{ 
         if(flag1 == 0)
         {
         
         strcpy(msg,"");
        sprintf(msg,"%d",count_vmx_exit);
        temp1 = strlen(msg);
        count = temp1;        
        copy_to_user(buf,msg, count);
          flag1 = 1;
          return count;   
        }
	else if(flag1 == 1) 
        {     
        flag1 = 0;
        return 0;
        } 
}  

int read_proc2(struct file *filp,char *buf,size_t count,loff_t *offp )
{
        if(flag2 == 0)
         {   
    
         strcpy(msg,"");
        sprintf(msg,"%d",count_vmx_irq);
        temp2 = strlen(msg);
        count = temp2;    
        copy_to_user(buf,msg, count);
          flag2 = 1;
          return count;   
        }   
        else if(flag2 == 1)  
        {    
        flag2 = 0;
        return 0;
        }  
}
int read_proc3(struct file *filp,char *buf,size_t count,loff_t *offp )
{
        if(flag3 == 0)
         {

         strcpy(msg,"");
        sprintf(msg,"%d",count_vmx_run);
        temp3 = strlen(msg);
        count = temp3;
        copy_to_user(buf,msg, count);
          flag3 = 1;
          return count;
        }
        else if(flag3 == 1)
        {
        flag3 = 0;
        return 0;
        }

}
int read_proc4(struct file *filp,char *buf,size_t count,loff_t *offp )
{
        if(flag4 == 0)
         {

         strcpy(msg,"");
        sprintf(msg,"%d",count_vmx_ept);
        temp4 = strlen(msg);
        count = temp4;
        copy_to_user(buf,msg, count);
          flag4 = 1;
          return count;
        }
        else if(flag4 == 1)
        {
        flag4 = 0;
        return 0;
        }

}



int write_proc1(struct file *filp,const char *buf,size_t count,loff_t *offp) 
{ 
       char *xxx;
       count_vmx_exit = simple_strtol(buf, &xxx, 10); 

	copy_from_user(msg,buf,count); 
	len1=count; 
	temp1=len1;
 
    return count; 
}  
int write_proc2(struct file *filp,const char *buf,size_t count,loff_t *offp)
{
       char *xxx;
       count_vmx_irq = simple_strtol(buf, &xxx, 10);

        copy_from_user(msg,buf,count);
        len2=count;
        temp2=len2;

    return count;
}
int write_proc3(struct file *filp,const char *buf,size_t count,loff_t *offp)
{
        char *xxx;
        count_vmx_run = simple_strtol(buf, &xxx, 10);

        copy_from_user(msg,buf,count);
        len3=count;
        temp3=len3;

    return count;
}
int write_proc4(struct file *filp,const char *buf,size_t count,loff_t *offp)
{
       char *xxx;
       count_vmx_ept = simple_strtol(buf, &xxx, 10);

        copy_from_user(msg,buf,count);
        len4=count;
        temp4=len4;

    return count;
}



struct file_operations proc_fops1 = { read: read_proc1, write: write_proc1 };
struct file_operations proc_fops2 = { read: read_proc2, write: write_proc2 };
struct file_operations proc_fops3 = { read: read_proc3, write: write_proc3 };
struct file_operations proc_fops4 = { read: read_proc4, write: write_proc4 };
 
void create_new_proc_entry()  
{ 
	parent=proc_mkdir(dirname,NULL); 
	proc_create("count_vmx_exit",0777,parent,&proc_fops1); 
        proc_create("count_vmx_irq",0777,parent,&proc_fops2);
        proc_create("count_vmx_run",0777,parent,&proc_fops3);
	proc_create("count_vmx_ept",0777,parent,&proc_fops4);

	msg=kmalloc(GFP_KERNEL,10*sizeof(char));
      
        // msg="kvm-here";
       // temp = strlen(msg);
}   

int proc_init (void) 
{  
	create_new_proc_entry();  
   return 0; 
}  

void proc_cleanup(void) 
{  
	remove_proc_entry("temp1",NULL);
}  

MODULE_LICENSE("GPL");  
module_init(proc_init); 
module_exit(proc_cleanup);  
