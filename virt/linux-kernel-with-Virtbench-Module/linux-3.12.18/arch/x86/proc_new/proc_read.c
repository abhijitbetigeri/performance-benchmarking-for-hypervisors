#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>

int count_vmx_exit=0,count_vmx_irq=0,count_vmx_run=0;
int len,temp;

int func_vmxexit(int num)
{
      count_vmx_exit=num;
      return 0;
}
EXPORT_SYMBOL(func_vmxexit);

int func_vmxirq(int num)
{
       count_vmx_irq=num;
       return 0;
}

EXPORT_SYMBOL(func_vmxirq);

int func_vmxrun(int num)
{
       count_vmx_run=num;
        return 0;
}
EXPORT_SYMBOL(func_vmxrun);


int len1,len2,len3,temp1,temp2,temp3;  
char *msg; char *dirname="virtbench"; 
struct proc_dir_entry *parent; 
int read_proc1(struct file *filp,char *buf,size_t count,loff_t *offp )  
{ 
         strcpy(msg,"");
        sprintf(msg,"%d",count_vmx_vmexit);
        temp1 = strlen(msg);
         if(count>temp1) 
	{ 
		count=temp1; 
	} 
	
        temp1=temp1-count; 
//	strcpy(msg,"");
  //      sprintf(msg,"%d",count_vmx_vmexit);
        copy_to_user(buf,msg, count);
          
	if(count==0) 
		temp1=len1;     
        
        return count; 
}  

int read_proc2(struct file *filp,char *buf,size_t count,loff_t *offp )
{
                 strcpy(msg,"");
        sprintf(msg,"%d",count_vmx_irq);
        temp2 = strlen(msg);

	if(count>temp2)
        {
                count=temp2;
        }
        temp2=temp2-count;
        copy_to_user(buf,msg, count);
        if(count==0)
                temp2=len2;
     return count;
}
int read_proc3(struct file *filp,char *buf,size_t count,loff_t *offp )
{
                strcpy(msg,"");
        sprintf(msg,"%d",count_vmx_vmrun);
        temp3 = strlen(msg);

        if(count>temp3)
        {
                count=temp3;
        }
        temp3=temp3-count;
        copy_to_user(buf,msg, count);
        if(count==0)
                temp3=len3;
     return count;
}

int write_proc1(struct file *filp,const char *buf,size_t count,loff_t *offp) 
{ 
	copy_from_user(msg,buf,count); 
	len1=count; 
	temp1=len1;
 
    return count; 
}  
int write_proc2(struct file *filp,const char *buf,size_t count,loff_t *offp)
{
        copy_from_user(msg,buf,count);
        len2=count;
        temp2=len2;

    return count;
}
int write_proc3(struct file *filp,const char *buf,size_t count,loff_t *offp)
{
        copy_from_user(msg,buf,count);
        len3=count;
        temp3=len3;

    return count;
}

struct file_operations proc_fops1 = { read: read_proc1, write: write_proc1 };
struct file_operations proc_fops2 = { read: read_proc2, write: write_proc2 };
struct file_operations proc_fops3 = { read: read_proc3, write: write_proc3 };
 
void create_new_proc_entry()  
{ 
	parent=proc_mkdir(dirname,NULL); 
	proc_create("count_vmx_exit",0,parent,&proc_fops1); 
        proc_create("count_vmx_irq",0,parent,&proc_fops2);
        proc_create("count_vmx_run",0,parent,&proc_fops3);
	msg=kmalloc(GFP_KERNEL,10*sizeof(char));
      
        // msg="kvm-here";
       // temp = strlen(msg);
}   

int proc_init (void) 
{  
	create_new_proc_entry();  
   return 0; 
}  

void proc_cleanup(void) 
{  
	remove_proc_entry("temp1",NULL);
}  

MODULE_LICENSE("GPL");  
module_init(proc_init); 
module_exit(proc_cleanup);  
